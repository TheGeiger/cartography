const React = require("react");
const ReactDOM = require("react-dom");
const cartoLib = require("./js/index");

let sean = null;
let palette = new cartoLib.palette();
let rdOnly = false;
let color = ["white", "white", "white", "white", "white", "white"];

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viewName: "project"
        };
    }

    showView() {
        if (sean) {
            sean.paintingMode(false); // always disable painting mode
        }
        switch (this.state.viewName) {
            case "project":
                return <ProjectView reRender={() => this.setState(this.state)}/>;
            case "paint":
                return <PaintView/>;
            case "question":
                return <QuestionView/>;
            default:
                return <ProjectView reRender={() => this.setState(this.state)}/>;
        }
    }

    changeView(e) {
        this.setState({
            viewName: e
        });
    }

    displayPaint() {
        if (sean !== null && rdOnly === false)
            return <div id="paint" onClick={() => this.changeView("paint")} className="div-block-8 paint">
                <div className="text-block-2 paintingIcon"></div>
            </div>
    }

    displayQuestion() {
        if (sean !== null && rdOnly === false)
            return <div id="question" onClick={() => this.changeView("question")} className="div-block-8 question">
                <div className="text-block-2"></div>
            </div>
    }

    render() {
        return <div className="div-block component">
            <div id="project" onClick={() => this.changeView("project")} className="div-block-8 option">
                <div className="text-block-2"></div>
            </div>
            {this.displayPaint()}
            {this.displayQuestion()}
            {this.showView()}
        </div>;
    }
}

class ProjectView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {readOnly: "off"}
    }

    importCarto() {
        if (sean !== null)
            console.log("no carto to export");
        else {
            let file = document.getElementById("file").files[0];
            if (file) {
                let reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = (evt) => {
                    let format = JSON.parse(evt.target.result);
                    sean = new cartoLib.cartography("div", format.height, format.width, format.img);
                    sean.importCarto(format);
                };
                reader.onerror = function (evt) {
                    console.log("error");
                };
                sean = ""; //et encore
                this.props.reRender();
            }
        }
    }

    exportCarto() {
        if (sean !== null) {
            sean.exportCarto();
        }
        else {
            console.log("no carto to export");
        }
    }

    rdOnly() {
        if (sean !== null) {
            if (this.state.readOnly === "off") {
                rdOnly = true;
                sean.block();
                this.setState({readOnly: "on"});
            } else {
                rdOnly = false;
                sean.unblock();
                this.setState({readOnly: "off"});
            }
            this.props.reRender();
        } else {
            console.log("No carto");
        }
    }

    scale(e) {
        if (sean !== null)
            sean.scale(e);
    }

    newCarto() {
        if (sean === null) {
            // create depending to image size
            let img = new Image();
            img.onload = function () {
                sean = new cartoLib.cartography("#svg", this.height * 10, this.width * 10, "../src/img/plan.png");
                // menu.createPalette(sean);
            };
            img.src = '../src/img/plan.png';
            sean = ""; // brek bwha ugly
            this.props.reRender();
        }
        // console.log(this.props.reRender);
    }

    setImage() {
        if (sean !== null) {
            let imgUrl = prompt("Image :", "https://www.image.ie/images/no-image.png");
            if (imgUrl == "" || imgUrl == null) {
                return;
            }
            let img = new Image();
            img.onload = function () {
                sean.changeImage(this.width, this.height, imgUrl);
            };
            img.src = imgUrl;
        }
    }

    render() {
        return (
            <div className="div-block">
                <div className="div-block-9">
                    <a href="#" className="button-2 w-button" onClick={() => this.newCarto()}><span style={{
                        "fontFamily": "'Fa solid 900', sans-serif",
                        "marginRight": 10 + "px"
                    }}></span>Nouveau</a>
                    <a href="#" className="button-2 w-button" onClick={() => this.exportCarto()}><span style={{
                        "fontFamily": "'Fa solid 900', sans-serif",
                        "marginRight": 10 + "px"
                    }}></span>Export</a>
                    <input type="file" name="file" id="file" className="inputfile" onChange={() => this.importCarto()}/>
                    <label htmlFor="file" className="button-2 w-button" style={{"fontWeight": "normal"}}><span style={{
                        "fontFamily": "'Fa solid 900', sans-serif",
                        "marginRight": 10 + "px"
                    }}></span>Import</label>
                </div>
                <div className="div-block-9">
                    <a href="#" className="button-2 w-button" onClick={() => this.setImage()}><span
                        style={{"fontFamily": "'Fa solid 900', sans-serif", "marginRight": 10 + "px"}}></span>Changement
                        d'image</a>
                    <a href="#" className="button-2 w-button" id={"rdOnly"} onClick={() => this.rdOnly()}><span
                        style={{"fontFamily": "'Fa solid 900', sans-serif", "marginRight": 10 + "px"}}></span>Lecture
                        seule
                        [{this.state.readOnly}]</a>
                    <a href="#" className="button-2 w-button" onClick={() => sean.getMapUg()}><span
                        style={{"fontFamily": "'Fa solid 900', sans-serif", "marginRight": 10 + "px"}}></span>Process</a>
                </div>
            </div>
        );
    }
}

class QuestionView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {rayon: "", ug: ""}
    }

    render() {
        return (
            <div className="div-block-14">
                <div className="div-block-15" style={{"marginLeft": 5 + "px"}}>
                    <div className="form-block-2 w-form">
                        <form id="email-form" name="email-form" data-name="Email Form" className="myform">
                            <div className="div-block-20" style={{"marginRight": 5 + "px"}}>
                                <div className="div-block-19">
                                    <input type="text"
                                           className="text-field-3 endtext w-input"
                                           maxLength="256"
                                           name="field"
                                           data-name="Field"
                                           placeholder="Name : "
                                           id="name-field" required="" style={{marginLeft: 0 + "px"}}/>
                                </div>
                                <div>
                                <textarea id="comment-field" name="field-3" placeholder="Comment :" maxLength="5000"
                                          className="textarea w-input"/>
                                </div>
                            </div>
                            <div className="div-block-21" style={{"flexDirection": "column"}}>
                                <select style={{
                                    "width": 100 + "%",
                                    "marginTop": "5px",
                                    "height": "40px",
                                    color: "white",
                                    "paddingLeft": "5px"
                                }} id={"rayon"} onChange={e=>this.setState({rayon: e.target.value})}>
                                    <option value="">Rayon :</option>
                                    <option value="00">00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </select>
                                <select style={{
                                    "width": 100 + "%",
                                    "marginTop": "1px",
                                    "height": "40px",
                                    color: "white",
                                    "paddingLeft": "5px"
                                }} id={"ug"} onChange={e=>this.setState({ug: e.target.value})}>
                                    <option value="">UG :</option>
                                    <option value="00">00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </select>
                                <a href="#" className="button-3 w-button" style={{
                                    "marginTop": "1px",
                                    "padding": "9px 0px",
                                    "width": "100%",
                                    "textAlign": "center"
                                }} onClick={() => sean.hilightItem(this.state.rayon, this.state.ug)}>find</a>
                            </div>
                            <div className="div-block-21" style={{"flexDirection": "column"}}>
                                <a href="#" className="button-3 w-button" style={{
                                    "marginTop": "5px",
                                    "padding": "9px 0px",
                                    "width": "90%",
                                    "textAlign": "center"
                                }} onClick={() => sean.addToSelectedShape()}>Ajout</a>
                                <a href="#" className="button-3 w-button" style={{
                                    "margin": "1px 00px",
                                    "padding": "9px 0px",
                                    "width": "90%",
                                    "textAlign": "center",
                                }} onClick={() => sean.deleteSelectedShape()}>Suppr</a>
                                <a href="#" className="button-3 w-button" style={{
                                    "margin": "1px 00px",
                                    "padding": "9px 0px",
                                    "width": "90%",
                                    "textAlign": "center"
                                }} onClick={() => sean.modifySelectedShape()}>Alt</a>
                            </div>
                            <div className="div-block-21" style={{"flexDirection": "column", width: 10 + "%"}}>
                                <input type="range" id="scale" min="0" max={sean.getMaxDeep()} step="1"
                                       style={{"transform": "rotate(270deg)", "width": 100 + "px"}} onChange={() => {
                                    if (sean !== null)
                                        sean.scale(document.getElementById("scale").value);
                                }}/>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="div-block-16">
                <pre style={{height: 100 + "%", "backgroundColor": "white", margin: 0 + "px"}} id={"metaShow"}>
                </pre>
                </div>
            </div>
        );
    }
};

class PaintView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "borderClass": ["", "", "", "", "", ""],
            "width": 100 + "%",
            "hide": {"display": "none"},
            "selectedId": ""
        };
    }

    componentDidMount() {
        document.getElementById("0").click();
    }

    setBorder(i) {
        let borders = ["", "", "", "", "", ""];
        borders[i] = "border";
        this.setState({borderClass: borders, "width": 80 + "%", "hide": {"display": "initial"}});
    }

    get selectedId() {
        for (let i in this.state.borderClass) {
            let b = this.state.borderClass[i];
            if (b === "border") {
                return i;
            }
        }
    }

    displayPalette() {
        if (!document.getElementById("palettePicker"))
            palette.createPalette(sean, (c) => {
                color[this.selectedId] = c;
                this.setState({state: this.state})
            });
    }

    render() {
        if (sean !== null) {
            this.displayPalette();
            return <div className="div-block paintingmenu">
                <Drawtools hide={this.state.hide}/>
                <div className="div-block-11" style={{"width": this.state.width}}>
                    <div className="w-form">
                        <form id="email-form" name="email-form" data-name="Email Form" className="form">
                            <div id={0} className={"div-block-12 " + this.state.borderClass[0]}
                                 style={{"backgroundColor": color[0]}}
                                 onClick={() => this.setBorder(0)} color={color[0]}/>
                            <input type="text" className="text-field w-input" maxLength="256" name="name"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[0], e.target.value)}}/>
                            <div id={1} className={"div-block-12 " + this.state.borderClass[1]}
                                 style={{"backgroundColor": color[1]}}
                                 onClick={() => this.setBorder(1)} color={color[1]}/>
                            <input type="text" className="text-field w-input" maxLength="256" name="name-2"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[1], e.target.value)}}/>
                            <div id={2} className={"div-block-12 " + this.state.borderClass[2]}
                                 style={{"backgroundColor": color[2]}}
                                 onClick={() => this.setBorder(2)} color={color[2]}/>
                            <input type="text" className="text-field endinput w-input" maxLength="256" name="name-3"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[2], e.target.value)}}/>
                        </form>
                    </div>
                    <div className="w-form">
                        <form id="email-form" name="email-form" data-name="Email Form" className="form">
                            <div id={3} className={"div-block-12 " + this.state.borderClass[3]}
                                 style={{"backgroundColor": color[3]}}
                                 onClick={() => this.setBorder(3)} color={color[3]}/>
                            <input type="text" className="text-field w-input" maxLength="256" name="name-6"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[3], e.target.value)}}/>
                            <div id={4} className={"div-block-12 " + this.state.borderClass[4]}
                                 style={{"backgroundColor": color[4]}}
                                 onClick={() => this.setBorder(4)} color={color[4]}/>
                            <input type="text" className="text-field w-input" maxLength="256" name="name-2"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[4], e.target.value)}}/>
                            <div id={5} className={"div-block-12 " + this.state.borderClass[5]}
                                 style={{"backgroundColor": color[5]}}
                                 onClick={() => this.setBorder(5)} color={color[5]} />
                            <input type="text" className="text-field endinput w-input" maxLength="256" name="name-3"
                                   placeholder="Default" onChange={(e)=>{sean.changeLabelOfLine(color[5], e.target.value)}}/>
                        </form>
                    </div>
                </div>
            </div>
        }
        return <div/>;
    }
}

const Drawtools = (props) => {
    return <div className="div-block-10" style={props.hide}>
        <div className="div-block-13">
            <div id="palette"/>
            <div className="text-block-3">
                <span style={{cursor: "pointer", marginLeft: 10 + "px"}} id={"pen"}
                      onClick={() => palette.penHandle()}></span>
                <span style={{cursor: "pointer", marginLeft: 20 + "px"}} id={"trash"}
                      onClick={() => palette.trashHandle()}></span>
            </div>
        </div>
    </div>;
};

ReactDOM.render(
    <Menu/>
    , document.getElementById('root')
);
