let d3 = require("./d3.js");
let colors = ["#ff2b6f", "#ffb157", "#8BC34A", "#CDDC39", "#009688", "#FF9800", "#2196F3", "#FFC107", "#9E9E9E", "#607D8B", "#FFEB3B", "#673AB7", "#03A9F4", "#F44336", "#3F51B5", "#795548", "#00BCD4", "#E91E63"];
const minimalSize = 5;

function download(content, filename, contentType) {
    if (!contentType) contentType = 'application/octet-stream';
    let a = document.createElement('a');
    let blob = new Blob([content], {'type': contentType});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.click();
}

let clicked = false;

function clickEvent() {
    if (clicked)
        clicked = false;
    setTimeout(clickEvent, 100);
}

function calcRatio(startw, starth, endw, endh) {
    return {w: (endw - startw) / startw, h: (endh - starth) / starth};
}

setTimeout(clickEvent, 100);

function moveChild(current, xmove, ymove) {
    current.childs.forEach((e) => {
        e.pos.x -= xmove;
        e.pos.y -= ymove;
        e.text.attr("y", e.pos.x).attr("x", e.pos.y)
        e.draw.attr("x", e.pos.x);
        e.draw.attr("y", e.pos.y);
        moveChild(e, xmove, ymove);
    });
}

function resizeChild(current, ratio) {
    current.childs.forEach((e) => {
        if (e.isPlaceEmpty(
            {
                x: ((ratio.w * (e.pos.x - current.pos.x)) + e.pos.x),
                y: ((ratio.h * (e.pos.y - current.pos.y)) + e.pos.y)
            },
            {width: (ratio.w * e.width) + e.width, height: (ratio.h * e.height) + e.height}
        )) {
            let w = (ratio.w * e.width) + e.width;
            let h = (ratio.h * e.height) + e.height;
            let x = ((ratio.w * (e.pos.x - current.pos.x)) + e.pos.x);
            let y = ((ratio.h * (e.pos.y - current.pos.y)) + e.pos.y);
            e.width = (w > 10 ? w : 7);
            e.height = (h > 10 ? h : 7);
            e.pos.x = (x - current.pos.x > 1 ? x : current.pos.x + 1);
            e.pos.y = (y - current.pos.y > 1 ? y : current.pos.y + 1);
            e.draw.attr("width", e.width);
            e.draw.attr("height", e.height);
            e.text.attr("y", e.pos.y).attr("x", e.pos.x);
            e.draw.attr("y", e.pos.y);
            e.draw.attr("x", e.pos.x);
            resizeChild(e, ratio);
        }
    });
}

// function syntaxHighlight(json) {
//     if (typeof json != 'string') {
//         json = JSON.stringify(json, undefined, 2);
//     }
//     json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
//     return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
//         var cls = 'number';
//         if (/^"/.test(match)) {
//             if (/:$/.test(match)) {
//                 cls = 'key';
//             } else {
//                 cls = 'string';
//             }
//         } else if (/true|false/.test(match)) {
//             cls = 'boolean';
//         } else if (/null/.test(match)) {
//             cls = 'null';
//         }
//         return '<span class="' + cls + '">' + match + '</span>';
//     });
// }

const borderSize = 25;
let shapeClicked = null; //default = baseShape
let Rect = class {
    constructor(svgContainer, x, y, father, color, width, height, id, image = null, data = null) {
        if (data === null)
            this.data = {
                name: "",
                ug: "",
                comment: "",
            };
        else
            this.data = data;
        this.id = id;
        this.pos = {x: x, y: y};
        this.father = father;
        this.width = width;
        this.height = height;
        this.svgContainer = svgContainer;
        this.color = color;
        this.g = this.svgContainer.append("g");
        this.draw = this.g.append("rect")
            .attr("x", this.pos.x).attr("y", this.pos.y)
            .attr("height", this.height).attr("width", this.width)
            .attr("fill", color).style("opacity", .9).attr("UG", "default").on("contextmenu", () => {
                let ug = prompt("UG : ", (this.draw.attr("UG") === "default" ? "default (" + this.ug + ")" : this.ug));
                if (ug == null || ug == "" || isNaN(ug.replace(";", ""))) {
                    ug = this.draw.attr("UG");
                }
                this.draw.attr("UG", ug);
            });
        if (image !== null) {
            shapeClicked = this;
            this.imgobj = this.svgContainer.append("svg:image")
                .attr("x", this.pos.x).attr("y", this.pos.y)
                .attr("height", this.height).attr("width", this.width)
                .attr("xlink:href", image).on("click", () => {
                    this.showMeta();
                    shapeClicked = this;
                });
        } else {
            this.text = this.g.append("text").attr("x", this.pos.x).attr("y", this.pos.y - borderSize - 5).attr("font-family", "Helvetica Neue").attr("font-size", "80").style("fill", "black").text(this.data.name);
            this.imgobj = null;
        }
        this.isResize = false;
        this.corner = "";
        this.calcBoxResize();
        this.clickable = true;
        this.draw.on("click", () => {
            setTimeout(() => {
                if (clicked === false) {
                    this.showMeta();
                    if (this.clickable === true) {
                        shapeClicked = this;
                        this.showSelection();
                    }
                }
            }, 10);
        });
        if (this.father !== null) {
            let drag_handler = d3.drag()
                .on("end", () => {
                    if (this.clickable === true) {
                        let draged = this.draw;
                        this.isResize = false;
                        this.width = Number(draged.attr("width"));
                        this.height = Number(draged.attr("height"));
                    }
                })
                .on("start", () => {
                    if (this.clickable === true) {
                        if (this.isCorner(d3.event.x, d3.event.y)) {
                            this.isResize = true;
                        }
                    }
                })
                .on("drag", () => {
                    if (this.clickable === true) {
                        if (this.isResize) { // RESIZE OBJECT
                            this.resize();
                        } else { // MOVE OBJECT
                            this.move();
                        }
                    }
                });
            drag_handler(this.draw);
            this.opacity = 1;
        }
        this.childs = [];
        this.borderColors = [];
        this.borderColor = "black";
    }

    updateName() {
        this.text.text(this.data.name);
    }

    showMeta() {
        // d3.select("#metaShow").text(JSON.stringify(this.doShowData(this), null, 2));
        d3.select("#metaShow").text(JSON.stringify(this.data, null, 2));
    }

    doShowData(current, acc) {
        acc = current.data;
        acc.childs = [];
        current.childs.forEach((c) => {
            acc.childs.push(this.doShowData(c, {}));
        });
        return acc;
    }

    setImage(width, height, image) {
        this.width = width;
        this.height = height;
        this.draw.attr("width", this.width).attr("height", this.height);
        if (this.imgobj === null) {
            this.imgobj = this.svgContainer.append("svg:image")
                .attr("x", this.pos.x).attr("y", this.pos.y)
                .attr("height", this.height).attr("width", this.width)
                .attr("xlink:href", image);
        } else {
            this.imgobj.attr("xlink:href", image);
        }
    }

    calcBoxResize() {
        let boxX = (Number(this.draw.attr("width")) * 0.07 <= 2 ? 2 : Number(this.draw.attr("width")) * 0.07);
        let boxY = (Number(this.draw.attr("height")) * 0.07 <= 2 ? 2 : Number(this.draw.attr("height")) * 0.07);

        this.resizeBox = (boxX + boxY) / 2;
    }

    getMousePos(cornerX, cornerY) {
        let pos = {left: false, right: false, up: false, down: false};
        let cornerYEmplacement = (d3.event.y - this.pos.y);
        let cornerXEmplacement = (d3.event.x - this.pos.x);
        if (d3.event.x + cornerXEmplacement - cornerX > 0) {
            pos.right = true;
        } else if (d3.event.x + cornerXEmplacement - cornerX < 0) {
            pos.left = true;
        }
        if (d3.event.y + cornerYEmplacement - cornerY < 0) {
            pos.up = true;
        } else if (d3.event.y + cornerYEmplacement - cornerY > 0) {
            pos.down = true;
        }
        return pos;
    }

    get ug() {
        let ug = this.draw.attr("UG");
        if (isNaN(ug.replace(";", ""))) {
            if (this.father == null) {
                return "default";
            } else {
                return this.father.ug;
            }
        } else {
            return ug;
        }
    }

    hilight(color = "blue") {
        this.borderColor = color;
        this.draw.attr("stroke-width", borderSize).attr("stroke", color);
    }

    noHilight() {
        this.draw.attr("stroke-width", 0).attr("stroke", "black");
    }

    resize() {
        let draged = this.draw;
        let nx = this.pos.x;
        let ny = this.pos.y;
        let wmove = (d3.event.x - nx);
        let hmove = (d3.event.y - ny);
        let ymove = this.pos.y;
        let xmove = this.pos.x;
        let w = Number(draged.attr("width"));
        let h = Number(draged.attr("height"));
        if (this.corner.includes("left")) {
            xmove = d3.event.x;
            wmove = (nx - d3.event.x) + w;
        }
        if (this.corner.includes("up")) {
            ymove = d3.event.y;
            hmove = (ny - d3.event.y) + h;
        }
        if (this.corner.includes("right")) {
            wmove = d3.event.x - nx;
            xmove = d3.event.x - wmove;
        }
        if (this.corner.includes("down")) {
            hmove = d3.event.y - ny;
            ymove = d3.event.y - hmove;
        }
        //////
        if (xmove < this.father.pos.x + 1) {
            xmove = this.father.pos.x + 2;
            wmove = w;
        }
        if (ymove < this.father.pos.y + 1) {
            ymove = this.father.pos.y + 2;
            hmove = h;
        }
        if (this.pos.x + wmove > this.father.pos.x + this.father.width) {
            wmove = this.father.width - (this.pos.x - this.father.pos.x) - 1;
        }
        if (this.pos.y + hmove > this.father.pos.y + this.father.height) {
            hmove = this.father.height - (this.pos.y - this.father.pos.y) + (this.father.y === 0 ? 27 : -1);
        }
        //////
        let xblock = 0;
        let yblock = 0;
        for (let index in this.childs) {
            let e = this.childs[index];
            let RWay = e.pos.x - this.pos.x + e.width + 1 > d3.event.x - this.pos.x;
            let BWay = e.pos.y - this.pos.y + e.height + 1 > d3.event.y - this.pos.y;
            let LWay = e.pos.x < xmove;
            let UWay = e.pos.y < ymove;
            if (RWay || BWay || LWay || UWay) {
                let ret = e.pos.x - this.pos.x + e.width + 1;
                if (RWay) {
                    wmove = (wmove > ret ? wmove : ret);
                    xblock = wmove;
                }
                if (LWay) {
                    xmove = e.pos.x - 1;
                    wmove = w;
                    xblock = wmove;
                }
                if (UWay) {
                    ymove = e.pos.y - 1;
                    hmove = h;
                    yblock = hmove;
                }
                ret = e.pos.y - this.pos.y + e.height + 1;
                if (BWay) {
                    hmove = (hmove > ret ? hmove : ret);
                    yblock = hmove;
                }
            }
        }
        let fatherChilds = this.father.childs;
        for (let index in fatherChilds) {
            let e = fatherChilds[index];
            if (e.id !== this.id) {
                let RWay = (d3.event.x + 1 > e.pos.x) && (this.pos.x + w < e.pos.x && (this.pos.y + h > e.pos.y && this.pos.y < e.pos.y + h));
                let BWay = (d3.event.y + 1 > e.pos.y) && (this.pos.y + h < e.pos.y && (this.pos.x + w > e.pos.x && this.pos.x < e.pos.x + w));
                let LWay = (d3.event.x - 1 < e.pos.x + e.width) && ((this.pos.x > e.pos.x + e.width && this.pos.y + h > e.pos.y) && (this.pos.y < e.pos.y + e.height));
                let UWay = (d3.event.y - 1 < e.pos.y + e.height) && ((this.pos.y > e.pos.y + e.height && this.pos.x + w > e.pos.x) && (this.pos.x < e.pos.x + e.width));
                if (RWay || BWay) {
                    if (RWay) {
                        let ret = (e.pos.x - this.pos.x) - 1;
                        if (xblock === 0 || ret < xblock) {
                            wmove = ret;
                            xblock = wmove;
                        }
                    }
                    if (BWay) {
                        let ret = (e.pos.y - this.pos.y) - 1;
                        if (yblock === 0 || ret < yblock) {
                            hmove = ret;
                            yblock = hmove;
                        }
                    }
                }
                else if (LWay || UWay) {
                    if (LWay) {
                        xmove = e.width + e.pos.x + 1;
                        wmove = w;
                        xblock = wmove;
                    } else if (UWay) {
                        ymove = e.height + e.pos.y + 1;
                        hmove = h;
                        yblock = hmove;
                    }
                }
            }
        }
        if (hmove < 7) {
            hmove = 7;
        }
        if (wmove < 7) {
            wmove = 7;
        }
        if (hmove + ymove < this.father.pos.y + this.father.height && xmove + wmove < this.father.pos.x + this.father.width) {
            this.pos.y = ymove;
            this.pos.x = xmove;
            let ratio = calcRatio(w, h, wmove, hmove);
            resizeChild(this, ratio);
            this.text.attr("y", ymove).attr("x", xmove)
            draged
                .attr("width", wmove).attr("height", hmove)
                .attr("y", ymove).attr("x", xmove);
            this.calcBoxResize();
        }
    }

    move() {
        let draged = this.draw;
        let xmove = d3.event.x - (this.width / 2);
        let ymove = d3.event.y - (this.height / 2);
        if ((Number(draged.attr("width")) + xmove > this.father.pos.x + this.father.width)) {
            xmove = this.father.pos.x + this.father.width - this.width - 1;
        } else if (xmove < this.father.pos.x) {
            xmove = this.father.pos.x + 1;
        }
        if (Number(draged.attr("height")) + ymove > this.father.pos.y + this.father.height) {
            ymove = this.father.pos.y + this.father.height - this.height - 1;
        } else if (ymove < this.father.pos.y) {
            ymove = this.father.pos.y + 1;
        }
        let xblock = 0;
        let yblock = 0;
        let fatherChilds = this.father.childs;
        for (let index in fatherChilds) {
            let e = fatherChilds[index];
            if (e.id !== this.id) {
                let left = (d3.event.x + (this.width / 2) + 1 > e.pos.x) // determine if the squar is at the left
                    && (this.pos.x < e.pos.x && this.pos.x < e.pos.x + e.width) // determine if the squar is at the left
                    && (this.pos.y < e.pos.y + e.height && this.pos.y + this.height > e.pos.y); // up and down case

                let right = ((d3.event.x - (this.width / 2)) - (e.pos.x + e.width) < 1)
                    && (this.pos.x > e.pos.x && this.pos.x > e.pos.x + e.width)
                    && (this.pos.y + this.height > e.pos.y && this.pos.y < e.pos.y + e.height);

                let up = ((d3.event.y - (this.height / 2)) - (e.pos.y + e.height) < 1)
                    && (this.pos.y > e.pos.y && this.pos.y > e.pos.y + e.height)
                    && (this.pos.x + this.width > e.pos.x && this.pos.x < e.pos.x + e.width);

                let down = (d3.event.y + (this.height / 2) > e.pos.y)
                    && (this.pos.y < e.pos.y && this.pos.y < e.pos.y + e.height + 1)
                    && (this.pos.x < e.pos.x + e.width && this.pos.x + this.width > e.pos.x);

                if (left) {
                    let ret = e.pos.x - this.width - 1;
                    if (xmove > e.pos.x + e.width + 1 && this.isPlaceEmpty({x: xmove, y: ymove}, {
                        width: this.width,
                        height: this.height
                    })) {
                        xblock = xmove;
                    } else if (xblock === 0 || ret < xblock) {
                        xmove = ret;
                        xblock = xmove;
                    }
                } else if (right) {
                    let ret = e.pos.x + e.width + 1;
                    if (xmove < e.pos.x - this.width - 1 && this.isPlaceEmpty({x: xmove, y: ymove}, {
                        width: this.width,
                        height: this.height
                    })) {
                        xblock = xmove;
                    } else if (xblock === 0 || ret > xblock) {
                        xmove = ret;
                        xblock = xmove;
                    }
                } else if (up) {
                    let ret = e.pos.y + e.height + 1;
                    if (ymove < e.pos.y - this.height - 1 && this.isPlaceEmpty({x: xmove, y: ymove}, {
                        width: this.width,
                        height: this.height
                    })) {
                        yblock = ymove;
                    } else if (yblock === 0 || ret > yblock) {
                        ymove = ret;
                        yblock = ymove;
                    }
                } else if (down) {
                    let ret = e.pos.y - this.height - 1;
                    if (ymove > e.pos.y + e.height + 1 && this.isPlaceEmpty({x: xmove, y: ymove}, {
                        width: this.width,
                        height: this.height
                    })) {
                        yblock = ymove;
                    } else if (yblock === 0 || ret < yblock) {
                        ymove = ret;
                        yblock = ymove;
                    }
                }
            }
        }
        for (let index in fatherChilds) {
            let e = fatherChilds[index];
            if (e.id !== this.id) {
                if (this.pos.x >= e.pos.x && this.pos.x <= e.pos.x + e.width && this.pos.y >= e.pos.y && this.pos.y <= e.pos.y + e.height) {
                    return;
                }
            }
        }
        if (!this.isPlaceEmpty({x: xmove, y: ymove}, {width: this.width, height: this.height})) {
            return;
        }
        if (ymove > this.father.pos.y && xmove > this.father.pos.x) {
            this.text.attr("y", ymove).attr("x", xmove)
            draged
                .attr("x", xmove)
                .attr("y", ymove);
            moveChild(this, this.pos.x - xmove, this.pos.y - ymove);
            this.pos.x = xmove;
            this.pos.y = ymove;
        }
    }

    isPlaceEmpty(pos, size) {
        let bros = this.father.childs;
        for (let index in bros) {
            let bro = bros[index];
            if (bro.id !== this.id) {
                let leftCoinInSquareBro = (pos.x >= bro.pos.x && pos.x <= bro.pos.x + bro.width) && (pos.y >= bro.pos.y && pos.y <= bro.pos.y + bro.height);
                let squareSizeInBro = (pos.x + size.width >= bro.pos.x && pos.x + size.width <= bro.width + bro.pos.x) && (pos.y >= bro.pos.y && pos.y < bro.height + bro.pos.y) ||
                    (pos.y + size.height >= bro.pos.y && pos.y + size.height <= bro.height + bro.pos.y) && (pos.x >= bro.pos.x && pos.x <= bro.width + bro.pos.x);
                let broInSquare = (pos.x <= bro.pos.x && pos.x + size.width >= bro.pos.x) && (pos.y <= bro.pos.y && pos.y + size.height >= bro.pos.y);
                if (leftCoinInSquareBro || squareSizeInBro || broInSquare) {
                    return false;
                }
            }
        }
        return true;
    }

    isCorner(x, y) {
        if (x - this.pos.x <= this.resizeBox && y - this.pos.y <= this.resizeBox) {
            this.corner = "up left";
            return (true);
        } else if (x - (this.pos.x + this.width) <= this.resizeBox && y - this.pos.y <= this.resizeBox) {
            this.corner = "up right";
            return (true);
        } else if ((this.pos.y + this.height) - y <= this.resizeBox && x - this.pos.x <= this.resizeBox) {
            this.corner = "down left";
            return (true);
        } else if ((this.pos.y + this.height) - y <= this.resizeBox && x - (this.pos.x + this.width) <= this.resizeBox) {
            this.corner = "down right";

            return (true);
        }
        return (false);
    }

    hideSelection() {
        this.draw.style("opacity", .9);
        // this.draw.attr("stroke-width", 0).attr("stroke", this.borderColor);
    }

    showSelection() {
        this.draw.style("opacity", .6);
        // this.draw.attr("stroke-width", borderSize).attr("stroke", this.borderColor);
    }

    opacityChange(opacity) {
        if (this.id !== 0) {
            this.clickable = opacity === 1;
            if (opacity !== 1) {
                this.opacity = opacity;
                this.draw.attr("stroke-width", 0).attr("stroke", "black");
                this.draw.style("opacity", this.opacity);
                this.draw.attr("fill", "gray");
            } else {
                this.draw.attr("fill", this.color);
                this.opacity = opacity;
                this.draw.style("opacity", 0.9);
            }
        }
    }

    deleteShape() {
        while (this.childs.length !== 0) {
            this.childs[0].deleteShape();
        }
        this.draw.remove();
        this.father.deleteChild(this.id);
    }

    deleteChild(id) {
        for (let index in this.childs) {
            let c = this.childs[index];
            if (c.id === id) {
                this.childs.splice(index, 1);
                break;
            }
        }
    }

    doFatherNumber(rect, acc) {
        return (rect.father === null ? acc : this.doFatherNumber(rect.father, ++acc))
    }

    getFatherNumber() {
        return (this.doFatherNumber(this, 0) % 17);
    }

    getChildSizeAndPos(size = 0.3) {
        let width = this.width * size;
        let height = this.height * size;
        if (width < 8 || height < 8) {
            return null;
        }
        let npos = this.getFreePos(width, height);
        if (npos === null) {
            size = Number((size - 0.03));
            return this.getChildSizeAndPos(size);
        } else {
            return npos;
        }
    }

    getFreePos(width, height) {
        if (width < 8 || height < 8) {
            return null;
        }
        if (this.childs.length === 0) {
            return ([this.pos.x + 1, this.pos.y + 1, width, height]);
        }
        for (let x = this.pos.x + 1; x + width + 1 < (this.pos.x + this.width); x = x + width + 1) {
            for (let y = this.pos.y + 1; y + height + 1 < (this.pos.y + this.height); y = y + height + 1) {
                let freeSpace = false;
                for (let i in this.childs) {
                    let c = this.childs[i];
                    let isXfree = (x + width > c.pos.x && x < c.pos.x + c.width);
                    let isYfree = (y + height > c.pos.y && y < c.pos.y + c.height);
                    if (isXfree && isYfree) {
                        freeSpace = false;
                        break;
                    }
                    freeSpace = true;
                }
                if (freeSpace === true) {
                    return ([x, y, width, height]);
                }
            }
        }
        return null;
    }

    getChildToJson(acc) {
        let childs = acc[acc.push({
            id: this.id,
            width: this.width,
            height: this.height,
            pos: this.pos,
            childs: [],
            ug: this.draw.attr("UG"),
            data: this.data
        }) - 1].childs;
        this.childs.forEach((e) => {
            e.getChildToJson(childs);
        });
    }

    alterChilds(to_alter) {
        this.childs.forEach((e) => {
            e.alterChilds(e);
        });
        for (let key in to_alter) {
            this[key] = to_alter[key];
        }
        this.hideSelection();
    };

    get shapePos() {
        return {
            x1: this.pos.x,
            y1: this.pos.y,
            x2: this.pos.x + this.width,
            y2: this.pos.y + this.height
        };
    }

};

const circleD = 22;
const stroke_width = 30;
let colorUsed = null;
let linesList = [];
let currentLine = {};
let activeColors = [];

let palette = class {
    constructor() {
        this.colorPad = ["#333333", "#ffffff", "#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666", "#ff2b6f", "#ffb157", "#8BC34A", "#CDDC39", "#009688", "#FF9800", "#2196F3", "#FFC107", "#9E9E9E", "#607D8B", "#FFEB3B", "#673AB7", "#03A9F4", "#F44336", "#3F51B5", "#795548"];
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.exist = false;
    }

    createPalette(sean, callback) {
        this.sean = sean;
        this.svgContainer = d3.select("#palette").append("svg").attr("id", "palettePicker").attr("width", "100%").attr("height", "60px").style("border", "none").append("g");
        this.colorPad.forEach((c, i) => {
            this.svgContainer.append("circle")
                .attr("cx", (i + 1) * (circleD + 4) / 2)
                .attr("cy", (i % 2 ? circleD / 2 + 12 : circleD / 2 + 34))
                .attr("r", circleD / 2)
                .attr("fill", c)
                .attr("class", "circle")
                .on("click", function () {
                    let object = d3.select(this);
                    let colorSelected = object.attr("fill");
                    let prevColor = d3.select(".border").attr("color");
                    if (activeColors.includes(colorSelected) === true && prevColor !== colorSelected) {
                        console.log("color used");
                        return;
                    }
                    let objects = d3.selectAll(".circle");
                    let colorIndex = activeColors.indexOf(prevColor);
                    if (colorIndex !== -1) {
                        activeColors.splice(colorIndex, 1);
                    }
                    objects.style("stroke", "none");      // set the line colour
                    colorUsed = colorSelected;
                    object.style("stroke", "black");      // set the line colour
                    d3.select(".border").style("background-color", colorUsed).attr("color", colorUsed);
                    linesList.forEach((l) => {
                        if (l.divId === document.getElementsByClassName("border")[0].id && l.color !== colorUsed) {
                            l.color = colorUsed;
                            l.elem.attr("stroke", colorUsed);
                            sean.shapes.forEach((s) => {
                                let found = -1;
                                s.borderColors.forEach((b, i) => {
                                    if (b === prevColor) {
                                        s.noHilight();
                                        s.hilight(colorUsed);
                                        found = i;
                                    }
                                });
                                if (found !== -1) {
                                    s.borderColors.splice(found, 1);
                                    s.borderColors.push(colorUsed);
                                }
                            });
                        }
                    });
                    activeColors.push(colorUsed);
                    callback(colorUsed);
                });
        });
        // this.colors = this.svgContainer.selectAll('circle').data(this.colorPad).enter().append("circle")
        //     .attr("cx", (d, i) => (i + 1) * (circleD + 4) / 2)
        //     .attr("cy", (d, i) => (i % 2 ? circleD / 2 + 12 : circleD / 2 + 34))
        //     .attr("r", () => circleD / 2)
        //     .attr("fill", (d) => d)
        //     .on("click", function () {
        //         colorUsed = d3.select(this).attr("fill");
        //         d3.select(".border").style({"background-color": "red"});
        //     });
        this.exist = true;
    }

    penHandle() {
        let obj = d3.select("#pen");
        if ("true" === obj.attr("actived")) {
            obj.attr("actived", "false");
            obj.style("color", "black");
            this.sean.paintingMode(false);
        } else {
            obj.attr("actived", "true");
            obj.style("color", "white");
            this.sean.paintingMode(true);
        }
    }

    trashHandle() {
        let object = d3.select(".border");
        let colorDel = object.attr("color");
        deleteLineOfColor(colorDel, this.sean.shapes);
        // linesList.forEach((l) => {
        //     if (l.color === colorDel && l.points.length !== 0) {
        //         // l.points.pop();
        //         // l.points.pop();
        //         // l.points.pop();
        //         // l.points.pop();
        //         // l.points.pop();
        //         // l.points.pop();
        //         // l.points.pop();
        //     }
        // });
        this.sean.drawLine();
    }
};

function deleteLineOfColor(colorDel, shapes) {
    for (let index in linesList) {
        let l = linesList[index];
        if (l.color === colorDel && l.points.length !== 0) {
            linesList.splice(index, 1);
            l.startShape.remove();
            l.endShape.remove();
            l.elem.remove();
            activeColors.splice(activeColors.indexOf(colorDel), 1);
            if (l === currentLine) {
                currentLine = null;
            }
            shapes.forEach((s) => {
                let borderIndex = s.borderColors.indexOf(colorDel);
                if (borderIndex !== -1) {
                    s.borderColors.splice(borderIndex, 1);
                    s.noHilight();
                }
            });
            return;
        }
    }
}

function lineExist(color) {
    for (let i in linesList) {
        let l = linesList[i];
        if (l.color === color) {
            return true;
        }
    }
    return false;
}

module.exports.palette = palette;

let cartography = class {
    constructor(element, height, width, image = null, oneObjectOpacity = true) {
        //create a svg container in the element "element" with defined width and height
        //element is a string
        this.image = image;
        this.height = height;
        this.width = width;
        this.id_index = 0;
        this.shapes = [];
        this.oneObjectOpacity = oneObjectOpacity;
        this.zoom = d3.zoom()
        // .translateExtent([[0, 10], [this.width, this.height]])
            .on("zoom", () => {
                this.svgContainer.attr("transform", d3.event.transform);
            });
        this.drag = d3.drag();
        this.drag.on('start', () => {
            if (lineExist(colorUsed)) {
                deleteLineOfColor(colorUsed, this.shapes);
            }
            // mettre un debut
            currentLine = {
                points: [],
                color: colorUsed,
                label: "",
                divId: document.getElementsByClassName("border")[0].id,
                start: {x: d3.event.x, y: d3.event.y},
                startShape: null,
                end: {x: d3.event.x, y: d3.event.y},
                endShape: null,
                shapesCrossed: [],
            };
            currentLine.startShape = this.svgContainer.append("circle").attr("cx", d3.event.x).attr("cy", d3.event.y).attr("r", 50).style("fill", "blue");
            currentLine.points.push({x: d3.event.x, y: d3.event.y});
            linesList.push(currentLine);
            this.drawLine();
        }).on('drag', () => {
            currentLine.points.push({x: d3.event.x, y: d3.event.y});
            currentLine.end = {x: d3.event.x, y: d3.event.y};
            if (currentLine.endShape !== null) {
                currentLine.endShape.remove();
            }
            currentLine.endShape = this.svgContainer.append("circle").attr("cx", d3.event.x).attr("cy", d3.event.y).attr("r", 50).style("fill", "red");
            this.drawLine();
        }).on('end', () => {
        });
        this.svgContainer = d3.select(element).append("svg").attr("width", window.innerWidth).attr("height", "87vh").style("border", "none").on("click", () => this.shapes.forEach(e => e.hideSelection())).on("contextmenu", function (d, i) {
            d3.event.preventDefault();
        }).call(this.zoom).on("dblclick.zoom", null);
        this.baseSvgContainer = this.svgContainer;
        this.svgContainer = this.svgContainer.append("g");
        this.baseMap = new Rect(this.svgContainer, 0, 31, null, "#CCCCCC", width, height, this.id_index++, image);
        this.shapes.push(this.baseMap);
        // this.svgContainer.attr("transform", "translate(-"+(this.width/10)+",-"+(this.height/10)+") scale(0.5)");
    }

    addToSelectedShape() {
        let childData = shapeClicked.getChildSizeAndPos();
        if (childData !== null) {
            let e = document.getElementById("rayon");
            let rayonField = e.options[e.selectedIndex].value;
            e = document.getElementById("ug");
            let ugField = rayonField + e.options[e.selectedIndex].value;
            let nameField = document.getElementById("name-field").value;
            let commentField = document.getElementById("comment-field").value;
            shapeClicked.childs.push(
                new Rect(this.svgContainer, childData[0], childData[1], shapeClicked, colors[shapeClicked.getFatherNumber()], childData[2], childData[3], this.id_index++, null, {
                    ug: ugField,
                    name: nameField,
                    comment: commentField
                })
            );
            this.shapes.push(shapeClicked.childs[shapeClicked.childs.length - 1]);
            d3.select("#scale").attr("max", this.getMaxDeep());
        } else {
            console.log("no space reaming");
        }
    }

    modifySelectedShape() {
        let e = document.getElementById("rayon");
        let rayonField = e.options[e.selectedIndex].value;
        e = document.getElementById("ug");
        let ugField = rayonField + e.options[e.selectedIndex].value;
        let nameField = document.getElementById("name-field").value;
        let commentField = document.getElementById("comment-field").value;
        shapeClicked.data.ug = ugField;
        shapeClicked.data.name = nameField;
        shapeClicked.data.comment = commentField;
        shapeClicked.showMeta();
        shapeClicked.updateName();
    }

    deleteId(id) {
        for (let i in this.shapes) {
            let s = this.shapes[i];
            if (s.id === id) {
                this.shapes.splice(i, 1);
                break;
            }
        }
    }

    deleteSelectedShape() {
        shapeClicked.deleteShape();
        this.deleteId(shapeClicked.id);
    }

    pointIsInShape(shape, point) {
        let shapePoints = shape.shapePos;
        if (point && (point.x > shapePoints.x1 && point.x < shapePoints.x2) && (point.y > shapePoints.y1 && point.y < shapePoints.y2)) {
            return (true);
        }
        return (false);
    }

    containShape(shapes, id) {
        for (let i in shapes) {
            let s = shapes[i];
            if (id === s.id) {
                return true;
            }
        }
        return false;
    }

    drawLine() {
        let lines;
        let l = d3.line().x(function (d) {
            return d.x;
        }).y(function (d) {
            return d.y;
        });
        lines = this.svgContainer.selectAll('.line').data(linesList);
        lines = lines.enter().append('path').attr("class", "line").attr("lineColor", d => d.color).attr("stroke", d => d.color).attr("stroke-width", stroke_width + "px").each(function (d) {
            return d.elem = d3.select(this);
        });

        function colorExist(s, color) {
            for (let index in s.borderColors) {
                if (s.borderColors[index] === color)
                    return true;
            }
        }

        if (currentLine) {
            currentLine.elem.attr("d", (d) => {
                this.shapes.forEach((s) => {
                    if (s.father !== null && s.father.father === null) {
                        if (this.pointIsInShape(s, d.points[d.points.length - 1])) {
                            s.hilight(d.color);
                            if (this.containShape(currentLine.shapesCrossed, s.id) === false)
                                currentLine.shapesCrossed.push(s);
                            if (!colorExist(s, d.color))
                                s.borderColors.push(d.color);
                        }
                    }
                });
                return l(d.points)
            });
        }
        lines.attr("d", (d) => {
            return l(d.points);
        });
        return lines.exit().remove();
    }

    paintingMode(painting) {
        if (painting) {
            this.block();
            this.baseSvgContainer.on(".zoom", null);
            this.drag(this.baseMap.imgobj);
        } else {
            this.unblock();
            this.baseSvgContainer.call(this.zoom);
            this.baseMap.imgobj.on(".drag", null);
        }
    }

    getMaxDeep() {
        let maxdepth = 0;
        this.shapes.forEach((e) => {
            let depth = e.getFatherNumber();
            if (maxdepth < depth)
                maxdepth = depth;
        });
        return maxdepth;
    }

    multipleOpacity(value) {
        value = Number(value);
        this.shapes.forEach((e, i) => {
            let fn = e.getFatherNumber();
            if (value === 0) {
                e.opacityChange(1);
            } else if (fn > value) {
                e.opacityChange(0);
            } else {
                let opacity = (fn / value);
                if (fn === value) {
                    opacity = 1;
                }
                e.opacityChange(opacity);
            }
        });
    }

    doOnChild(current, callback) {
        current.childs.forEach((c) => {
            this.doOnChild(c, callback)
        });
        callback(current);
    }

    doOnShapes(callback) {
        this.shapes.forEach((s) => {
            if (s.father !== null && s.father.father === null) {
                this.doOnChild(s, callback)
            }
        });
    }

    block() {
        this.doOnShapes(s => s.alterChilds({clickable: false}))
    }

    unblock() {
        this.doOnShapes(s => s.alterChilds({clickable: true}))
    }

    signleOpacity(value) {
        value = Number(value);

        this.shapes.forEach((e, i) => {
            let fn = e.getFatherNumber();
            if (value === 0) {
                e.opacityChange(1);
            } else {
                let opacity = (fn / value);
                opacity = opacity > 1 ? value / fn : opacity;
                e.opacityChange(opacity);
            }
        });
    }

    scale(value) {
        if (this.oneObjectOpacity) {
            this.signleOpacity(value);
        } else {
            this.multipleOpacity(value);
        }
    }

    createChild(father, childs) {
        childs.forEach((e) => {
            let current = father.childs[father.childs.push(new Rect(this.svgContainer, e.pos.x, e.pos.y, father, colors[father.getFatherNumber()], e.width, e.height, this.id_index++, null, e.data)) - 1];
            current.draw.attr("UG", current.ug);
            this.shapes.push(current);
            this.createChild(current, e.childs);
        });
    }

    importCarto(format) {
        this.createChild(this.shapes[0], format.childs);
    }

    getFileName() {
        let person = prompt("Please enter your name", "Export.json");
        if (person == "" || person == null) {
            return this.getFileName();
        }
        return person;
    }

    exportCarto() {
        let format = {height: this.height, width: this.width, img: this.image, childs: []};
        this.shapes.forEach((s) => {
            if (s.father !== null && s.father.father === null) {
                s.getChildToJson(format.childs);
            }
        });
        download(JSON.stringify(format, null, 2), this.getFileName(), 'application/json')
    }

    changeImage(width, height, img) {
        this.baseSvgContainer.attr("width", width * 10);
        this.baseSvgContainer.attr("height", height * 10);
        this.shapes[0].setImage(width, height, img);
    }

    // draw a rectangle at position x, y with defined height and width in svg container svgContainer
    drawRect(x, y, father, color, width = 30, height = 30) {
        return this.shapes[this.shapes.push(new Rect(this.svgContainer, x, y, father, color, width, height, this.id_index++)) - 1];
    }

    hilightItem(rayon, ug) {
        this.shapes.forEach((s) => {
            if ((s.data.ug[2] === ug[0] && s.data.ug[3] === ug[1]) &&
                (s.data.ug[0] === rayon[0] && s.data.ug[1] === rayon[1])) {
                s.hilight();
            } else {
                s.noHilight();
            }
        });
    }

    getMapUg () {
        let paths = [];
        linesList.forEach((l) => {
            paths.push({name: l.label, shapes: l.shapesCrossed});
        });
    }

    changeLabelOfLine(color, label) {
        linesList.forEach((l)=>{
            if (l.color === color) {
                l.label = label;
            }
        });
    }
};

module.exports.cartography = cartography;